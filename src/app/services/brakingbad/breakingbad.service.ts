import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BreakingbadService {

  //? Escribir URL a la cual se hará la petición.
  public URL = 'https://www.breakingbadapi.com/api';

  //? Inyectar el método HTTPCLIENT en el constructor 
  constructor( private _http : HttpClient ) { }

  //? Escribir mi petición

  getPersonajes() {

    const uri = `${this.URL}/characters`;

    return this._http.get(uri);
  }

  getEpisodios(){

    const epi = `${this.URL}/episodes`;
    
    return this._http.get(epi);
  }

  getquotes(){

    const quo = `${this.URL}/quotes`;
    
    return this._http.get(quo);
  }

  getdeaths(){

    const deaths = `${this.URL}/death`;
    
    return this._http.get(deaths);
  }
}
