import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ComponenteUnoComponent } from './components/componente-uno/componente-uno.component';

//! Invocar el RouterModule
import { RouterModule } from '@angular/router';
//! Invocar el AppRoutes
import { ROUTES } from './app.routes';
import { NavBarComponent } from './components/share/nav-bar/nav-bar.component';

import { HttpClientModule } from '@angular/common/http';
import { ComponenteDosComponent } from './components/componente-dos/componente-dos.component';
import { ComponenteTresComponent } from './components/componente-tres/componente-tres.component';
import { ComponenteCuatroComponent } from './components/componente-cuatro/componente-cuatro.component';
import { RickMortyComponent } from './components/rick-morty/rick-morty.component';
import { CodeEnigmaComponent } from './components/code-enigma/code-enigma.component';

import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ComponenteUnoComponent,
    NavBarComponent,
    ComponenteDosComponent,
    ComponenteTresComponent,
    ComponenteCuatroComponent,
    RickMortyComponent,
    CodeEnigmaComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxSpinnerModule,
    RouterModule.forRoot(ROUTES, {useHash: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
