import { Routes } from "@angular/router";
import { CodeEnigmaComponent } from "./components/code-enigma/code-enigma.component";
import { ComponenteCuatroComponent } from "./components/componente-cuatro/componente-cuatro.component";
import { ComponenteDosComponent } from "./components/componente-dos/componente-dos.component";
import { ComponenteTresComponent } from "./components/componente-tres/componente-tres.component";
import { ComponenteUnoComponent } from "./components/componente-uno/componente-uno.component";
import { HomeComponent } from "./components/home/home.component";
import { RickMortyComponent } from "./components/rick-morty/rick-morty.component";

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'componente/uno', component: ComponenteUnoComponent },
    { path: 'dos', component: ComponenteDosComponent },
    { path: 'tres', component: ComponenteTresComponent },
    { path: 'cuatro', component: ComponenteCuatroComponent },
    { path: 'rick-morty', component: RickMortyComponent },
    { path: 'code-enigma', component: CodeEnigmaComponent },
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' },
    { path: '###', pathMatch: 'full', redirectTo: 'dos' },
    { path: '###', pathMatch: 'full', redirectTo: 'tres' },
    { path: '###', pathMatch: 'full', redirectTo: 'cuatro' },
    { path: '###', pathMatch: 'full', redirectTo: 'rick-morty' },
    { path: '###', pathMatch: 'full', redirectTo: 'code-enigma' }
];

