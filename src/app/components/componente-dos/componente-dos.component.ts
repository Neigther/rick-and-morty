import { Component, OnInit } from '@angular/core';
import { BreakingbadService } from 'src/app/services/brakingbad/breakingbad.service';

@Component({
  selector: 'app-componente-dos',
  templateUrl: './componente-dos.component.html',
  styleUrls: ['./componente-dos.component.css']
})
export class ComponenteDosComponent implements OnInit {

  public episodes : any[] = [];

  constructor( private _breackingBad : BreakingbadService ) { }

  ngOnInit(): void {
    this.getTodosEpisodios();
  }

  getTodosEpisodios(){
    this.episodes = [];
    this._breackingBad.getEpisodios()
    .subscribe((data: any) => {
      console.log(data);
      this.episodes = data;
      console.log('EPISODIOS SETEADOS',this.episodes);
    })
  }
}
