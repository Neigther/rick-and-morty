import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeEnigmaComponent } from './code-enigma.component';

describe('CodeEnigmaComponent', () => {
  let component: CodeEnigmaComponent;
  let fixture: ComponentFixture<CodeEnigmaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodeEnigmaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeEnigmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
