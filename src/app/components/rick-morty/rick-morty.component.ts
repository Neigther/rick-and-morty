import { Component, OnInit } from '@angular/core';
import { RickandmortyService } from 'src/app/services/rickandmorty/rickandmorty.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-rick-morty',
  templateUrl: './rick-morty.component.html',
  styleUrls: ['./rick-morty.component.css']
})

export class RickMortyComponent implements OnInit {

  public personajes : any[] = [];

  constructor( private _RickandMorty : RickandmortyService,  private spinner: NgxSpinnerService ) { }

  ngOnInit(): void {
            /** spinner starts on init */
            this.spinner.show();
 
            setTimeout(() => {
              /** spinner ends after 5 seconds */
              this.spinner.hide();
            }, 5000);
    this.getTodosPersonajes();
  }

  getTodosPersonajes(){
    this.personajes = [];
    this._RickandMorty.getPersonajesRM()
    .subscribe((data: any) => {
      console.log(data);
      this.personajes = data;
      console.log('Personajes SETEADOS',this.personajes);
    })
  }
}