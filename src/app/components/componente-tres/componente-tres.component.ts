import { Component, OnInit } from '@angular/core';
import { BreakingbadService } from 'src/app/services/brakingbad/breakingbad.service';

@Component({
  selector: 'app-componente-tres',
  templateUrl: './componente-tres.component.html',
  styleUrls: ['./componente-tres.component.css']
})
export class ComponenteTresComponent implements OnInit {

  public Quotes : any[] = [];

  constructor( private _breackingBad : BreakingbadService ) { }

  ngOnInit(): void {
    this.getTodosquotes();
  }

  getTodosquotes(){
    this.Quotes = [];
    this._breackingBad.getquotes()
    .subscribe((data: any) => {
      console.log(data);
      this.Quotes = data;
      console.log('QUOTES SETEADOS',this.Quotes);
    })
  }
}
