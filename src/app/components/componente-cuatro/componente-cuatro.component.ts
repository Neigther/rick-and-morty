import { Component, OnInit } from '@angular/core';
import { BreakingbadService } from 'src/app/services/brakingbad/breakingbad.service';

@Component({
  selector: 'app-componente-cuatro',
  templateUrl: './componente-cuatro.component.html',
  styleUrls: ['./componente-cuatro.component.css']
})
export class ComponenteCuatroComponent implements OnInit {
  
  public Deaths : any[] = [];

  constructor( private _breackingBad : BreakingbadService ) { }

  ngOnInit(): void {
    this.getTodosdeaths();
  }

  getTodosdeaths(){
    this.Deaths = [];
    this._breackingBad.getdeaths()
    .subscribe((data: any) => {
      console.log(data);
      this.Deaths = data;
      console.log('DEATHS SETEADOS',this.Deaths);
    })
  }
}
