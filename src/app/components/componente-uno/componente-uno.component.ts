import { Component, OnInit } from '@angular/core';
import { BreakingbadService } from 'src/app/services/brakingbad/breakingbad.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.css']
})
export class ComponenteUnoComponent implements OnInit {

  public personajes : any[] = [];

  constructor( private _breackingBad : BreakingbadService, private spinner: NgxSpinnerService ) { }

  ngOnInit(): void {
        /** spinner starts on init */
        this.spinner.show();
 
        setTimeout(() => {
          /** spinner ends after 5 seconds */
          this.spinner.hide();
        }, 5000);
    this.getTodosPersonajes();
  }

  getTodosPersonajes(){
    this.personajes = [];
    this._breackingBad.getPersonajes()
    .subscribe((data: any) => {
      console.log(data);
      this.personajes = data;
      console.log('Personajes SETEADOS',this.personajes);
    })
  }

}
